<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/music/index', 'SongController@index')->name('music/index');
Route::get('/music/create', 'SongController@create')->name('music/create');
Route::post('/music/store', 'SongController@store')->name('music/store');
Route::get('/music/edit/{id}', 'SongController@edit')->name('music/edit/{id}');
Route::get('/music/view/{id}', 'SongController@view')->name('music/view/{id}');
Route::post('/music/update', 'SongController@update')->name('music/update');
Route::post('/music/dataTables', 'SongController@dataTables')->name('music/dataTables');
Route::get('/music/delete/{id}', 'SongController@delete')->name('music/delete/{id}');
