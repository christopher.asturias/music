@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif


                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h5 class="mr-auto">Music Lists</h5>
                        <div class="btn-group" role="group">
                            <a href="{{ route('music/create') }}" class="btn btn-primary">Create new Songs</a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Artist</th>
                                <th scope="col">Created Date</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($songs as $song)
                                <tr>
                                    <th scope="row"> <a href="{{ url('music/view', $song->id) }}" >{{$song->title}}</a> </th>
                                    <th scope="row"> {{$song->artist}} </th>
                                    <th scope="row"> {{$song->created_at}} </th>
                                    <th scope="row"> 
                                        <a href="{{ url('music/edit', $song->id) }}" class="btn btn-primary">Edit</a>
                                        <a href="{{ url('music/delete', $song->id) }}" class="btn btn-danger">Delete</a>
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>


@endsection
