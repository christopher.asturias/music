@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Create new Songs') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('music/store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="title" class="col-md-2 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-8">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}"  autocomplete="title" >

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="artist" class="col-md-2 col-form-label text-md-right">{{ __('Artist') }}</label>

                            <div class="col-md-8">
                                <input id="artist" type="text" class="form-control @error('artist') is-invalid @enderror" name="artist" value="{{ old('artist') }}"  autocomplete="artist" >

                                @error('artist')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lyrics" class="col-md-2 col-form-label text-md-right">{{ __('Lyrics') }}</label>

                            <div class="col-md-8">
                                <textarea id="lyrics" type="text" class="form-control @error('lyrics') is-invalid @enderror" name="lyrics" >
                                    {{ old('lyrics') }}
                                </textarea>
                                @error('lyrics')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                                <a href="{{ route('music/index') }}" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        CKEDITOR.replace( 'lyrics' ); 
    });
</script> 

@endsection
