@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Songs') }}</div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="title" class="col-md-2 col-form-label ">{{ __('Title:') }}</label>

                        <div class="col-md-6">
                            {{ $song->title }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="artist" class="col-md-2 col-form-label ">{{ __('Artist:') }}</label>

                        <div class="col-md-6">
                            {{ $song->artist }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="lyrics" class="col-md-2 col-form-label ">{{ __('Lyrics:') }}</label>

                        <div class="col-md-6">
                            {!! $song->lyrics !!}
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <a href="{{ route('music/index') }}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
