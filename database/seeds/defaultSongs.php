<?php

use Illuminate\Database\Seeder;
use App\user;
use App\Songs;

class defaultSongs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        user::create([
            'id' => 1,
            'name' => 'admin',
            'email' => 'admin@yahoo.com',
            'password' => Hash::make('123123'),
        ]);

        Songs::create([
            'id' => 1,
            'title' => 'A Perfect Christmas',
            'artist' => 'Jose Mary Chan',
            'lyrics' => "Lyrics
            My idea of a perfect Christmas
            Is to spend it with you
            In a party or dinner for two
            Anywhere would do
            Celebrating the yuletide season
            Always lights up our lives
            Simple pleasures are made special too
            When they're shared with you
            Looking through some old photographs
            Faces and friends we'll always remember
            Watching busy shoppers rushing about
            In the cool breeze of December
            Sparkling lights all over town
            Children's carols in the air
            By the Christmas tree
            A shower of stardust on your hair
            I can't think of a better Christmas
            Than my wish coming true
            And my wish is that you'd let me spend
            My whole life with you
            Looking through some old photographs
            Faces and friends we'll always remember
            Watching busy shoppers rushing about
            In the cool breeze of December
            Sparkling lights all over town
            Children's carols in the air
            By the Christmas tree
            A shower of stardust on your hair
            I can't think of a better Christmas
            Than my wish coming true
            And my wish is that you'd let me spend
            My whole life with you
            My idea of a perfect Christmas
            Is spending it with you",
        ]);

        Songs::create([
            'id' => 2,
            'title' => 'The Sound of Life',
            'artist' => 'Jose Mary Chan',
            'lyrics' => "
                Can you hear the sound of life
                Heard in the laughter of children at play
                Can you hear the sound of voices sing
                Feel the magic and joy they bring
                Can you hear the laughter? Can you hear the music?
                Sing with your heart, it's the song of life
                Can you hear it, can you feel it?
                It's the magic in your heart
                It's the music, the sound of life.
                Can you hear the Christmas bells ring
                And the sound of the carolers too?
                Can you hear the message far and near
                Merry Christmas, the Lord is here.
                Can you hear the laughter? Can you hear the music?
                Sing with your life, it's the song of life
                Can you hear it, can you feel it?
                Sing the message loud and clear
                Merry Christmas, the Lord is here.
                Lalalalalalala...
                Can you hear the laughter? Can you hear the music?
                Sing with your heart, it's the song of life.
                Can you hear it? Can you feel it?
                It's the magic in your heart,
                It's the music, the sound of life.
                Can you hear the message far and near
                Merry Christmas the Lord is near.
                Can you hear the laughter? Can you hear the music?
                Sing with your heart, it's the song of life.
                Can you hear it? Can you feel it?
                It's the magic in your heart,
                It's the music, the sound of life.
            ",
        ]);
            
        Songs::create([
            'id' => 3,
            'title' => 'Namamasko Po',
            'artist' => 'Parokya ni Edgar',
            'lyrics' => "
                Pagpasensyahan nyo na kung kami'y nanggugulo
                Ganyan lang talaga tuwing kami'y namamasko
                Walang pakialam kung saan mapadaan
                Basta't mayroon mga mapagkakakitaan
                Mayroon bitbit na gitara para sa aming pagkanta
                Mayroong dalang tubo baka may manghabol na aso
                Tanggap naman namin na kami ay sintunado
                Pero naman lolo please lang 'wag kang mangbabato
                Paminsan-minsan lang naman ang trip na 'to
                Paminsan-minsan lang naman din ang Pasko
                Hindi na dapat pinag-iisipan pa
                Lahat tayo ay dapat magsaya
                Hapon palang nagkakaroling na kami
                Tinatanggap namin maging dyis man o bente
                'Di na kailangan pang mahusay ang pagkakanta
                Basta't sabay sabay kami at sama sama
                Magkano na kaya ang aming naipon
                Ayos lang kahit magkano basta't may pang-inom
                Ilang bahay na lang mayroon nang pampulutan
                Tapos sa bahay ko kami mag-iinuman
                Paminsan-minsan lang naman ang trip na 'to
                Paminsan-minsan lang naman din ang Pasko
                Hindi na dapat pinag-iisipan pa
                Lahat tayo ay dapat magsaya
                'Di na yata magsasawa
                Sa'ming pagsasama-sama
                At sa aming pagkanta
                Asahan na maraming kita
                At kami ay sigurado
                Na panandalian lamang 'to
                Alam naman namin na hindi nga araw araw ang Pasko
            ",
        ]);
    }
}
