<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Songs;
use DataTables;

class SongController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth');
    }

    public function index()
    {
        $songs = Songs::select('*')->get();
        
        return view('music.index', ['songs' => $songs]);
    }

    public function create()
    {
        return view('music/create');
    }

    public function store(Request $request)
    {
        $employee = Songs::create([
            'title' => $request->title,
            'artist' => $request->artist,
            'lyrics' => $request->lyrics,
        ]);

        return redirect()->route('music/index')->with('success','New song is created.');
    }

    public function edit($id)
    {
        $song = Songs::where('id', $id)->first();

        return view('music.edit', [ 'song' => $song ]);
    }

    public function update(Request $request)
    {
        $song = Songs::where('id', $request->id)->first();
        $song->update([
            'title' => $request->title,
            'artist' => $request->artist,
            'lyrics' => $request->lyrics,
        ]);

        return redirect()->route('music/index')->with('success','Song is succesfully updated.');
    }

    public function delete($id)
    {
        Songs::where('id', $id)->delete();

        return redirect()->route('music/index')->with('error','Songs is deleted.');
    }

    public function view($id)
    {
        $song = Songs::where('id', $id)->first();

        return view('music.view', [ 'song' => $song ]);
    }

    public function dataTables(Request $request)
    {
        // \Log::info('adada');
        // return Datatables::of($data)
                
        //     ->addColumn('action', function($row){

        //             $btn = "<a href='' class='btn btn-primary'>Edit</a>
        //             <a href='' class='btn btn-danger'>Delete</a>";
                    

        //             return $btn;
        //     })
        //     ->rawColumns(['action'])
        //     ->make(true);
        return Datatables::of(Songs::query())->make(true);
        
    }
}
